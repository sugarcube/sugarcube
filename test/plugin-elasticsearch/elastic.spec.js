import ElasticDo from "../../packages/plugin-elasticsearch/lib/elastic";

describe("The elasticsearch context", () => {
  it("has a do context", () => ElasticDo.should.be.a("function"));
});
