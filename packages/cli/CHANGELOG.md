# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)


### Features

* **cli:** Added a command argument to list all available plugins. ([6d1b8c5](https://gitlab.com/sugarcube/sugarcube/commit/6d1b8c5))
* **cli:** Added command arguments to toggle features. ([825f9f3](https://gitlab.com/sugarcube/sugarcube/commit/825f9f3))
* **core,cli:** Added basic facilities for feature toggles. ([8f10385](https://gitlab.com/sugarcube/sugarcube/commit/8f10385))




<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)


### Features

* **cli:** Specify queries in config files. ([b1e5064](https://gitlab.com/sugarcube/sugarcube/commit/b1e5064)), closes [#4](https://gitlab.com/sugarcube/sugarcube/issues/4)
* **core:** Provide all available plugins to a plugin. ([cf2ebdf](https://gitlab.com/sugarcube/sugarcube/commit/cf2ebdf))
* **core,cli:** Added support for persistent cache. ([ea0429e](https://gitlab.com/sugarcube/sugarcube/commit/ea0429e))




<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)


### Features

* **core:** Supply plugins with a persistent stats object. ([0374b99](https://gitlab.com/sugarcube/sugarcube/commit/0374b99))
* **plugin-mail:** Added the mail_diff_stats plugin. ([c9dd267](https://gitlab.com/sugarcube/sugarcube/commit/c9dd267))




<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)


### Features

* **core:** Added warn log target. ([8e7f0b4](https://gitlab.com/sugarcube/sugarcube/commit/8e7f0b4))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)


### Bug Fixes

* Fixed the plugins that got broken by removing bluebird. ([73a9603](https://gitlab.com/sugarcube/sugarcube/commit/73a9603))
* Handle unhandled promise rejections. ([ae64edc](https://gitlab.com/sugarcube/sugarcube/commit/ae64edc))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/cli

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)


### Bug Fixes

* Handle unhandled promise rejections. ([ae64edc](https://gitlab.com/sugarcube/sugarcube/commit/ae64edc))




<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.0.0...v0.1.0) (2017-10-08)




**Note:** Version bump only for package @sugarcube/cli
