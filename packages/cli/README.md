# The SugarCube command line interface

A command line interface to SugarCube. See the SugarCube tutorial on how to
use it, or print the help output.

`sugarcube -h`

## Installation

    npm install -S @sugarcube/cli
