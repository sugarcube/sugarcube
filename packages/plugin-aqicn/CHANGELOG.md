# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)




**Note:** Version bump only for package @sugarcube/plugin-aqicn

<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)




**Note:** Version bump only for package @sugarcube/plugin-aqicn

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-aqicn

<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)




**Note:** Version bump only for package @sugarcube/plugin-aqicn

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)


### Bug Fixes

* Fixed the plugins that got broken by removing bluebird. ([73a9603](https://gitlab.com/sugarcube/sugarcube/commit/73a9603))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-aqicn

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-aqicn

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.0.0...v0.1.0) (2017-10-08)


### Features

* Imported the aqicn plugin. ([0344a3e](https://gitlab.com/sugarcube/sugarcube/commit/0344a3e))
