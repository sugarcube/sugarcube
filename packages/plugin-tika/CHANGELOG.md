# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)


### Bug Fixes

* **plugin-tika:** Normalize text and meta in a more sane way. ([524bfd0](https://gitlab.com/sugarcube/sugarcube/commit/524bfd0))




<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)




**Note:** Version bump only for package @sugarcube/plugin-tika

<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.5.1...v0.6.0) (2018-02-01)


### Features

* **plugin-tika:** Added the tika_export plugin. ([afe00b3](https://gitlab.com/sugarcube/sugarcube/commit/afe00b3))
* **plugin-tika:** Allow the tika_location plugin to parse more than one field. ([f2205f6](https://gitlab.com/sugarcube/sugarcube/commit/f2205f6))




<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.5.0...v0.5.1) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-tika

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)


### Features

* **plugin-tika:** In debug mode log all urls that are parsed by Tika. ([b4fea88](https://gitlab.com/sugarcube/sugarcube/commit/b4fea88))




<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)




**Note:** Version bump only for package @sugarcube/plugin-tika

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)


### Features

* **plugin-tika:** Added the tika_location plugin. ([89ff82e](https://gitlab.com/sugarcube/sugarcube/commit/89ff82e))
* **plugin-tika:** Extract text and meta data from all links. ([f119d28](https://gitlab.com/sugarcube/sugarcube/commit/f119d28))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-tika

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)


### Features

* **plugin-tika:** Added the tika_location plugin. ([89ff82e](https://gitlab.com/sugarcube/sugarcube/commit/89ff82e))
* **plugin-tika:** Extract text and meta data from all links. ([f119d28](https://gitlab.com/sugarcube/sugarcube/commit/f119d28))




<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.0.0...v0.1.0) (2017-10-08)


### Features

* Imported the tika plugin. ([9914084](https://gitlab.com/sugarcube/sugarcube/commit/9914084))
