# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-test/compare/v0.8.0...v0.9.0) (2018-03-30)




**Note:** Version bump only for package @sugarcube/plugin-test

<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-test/compare/v0.7.0...v0.8.0) (2018-03-03)


### Features

* **plugin-test:** Added the test_generate plugin. ([d144e39](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-test/commit/d144e39))
