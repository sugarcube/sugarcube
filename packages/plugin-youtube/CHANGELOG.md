# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)




**Note:** Version bump only for package @sugarcube/plugin-youtube

<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)




**Note:** Version bump only for package @sugarcube/plugin-youtube

<a name="0.7.0"></a>
# [0.7.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.6.1...v0.7.0) (2018-02-02)


### Features

* Consistently use _sc_media over _sc_downloads. ([9c92935](https://gitlab.com/sugarcube/sugarcube/commit/9c92935))
* **plugin-youtube:** Removed deprecated youtube_download plugin. ([cd9abad](https://gitlab.com/sugarcube/sugarcube/commit/cd9abad))
* **plugin-youtube:** Store the link to the video as url type in _sc_media as well. ([18e4f9a](https://gitlab.com/sugarcube/sugarcube/commit/18e4f9a))




<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.5.1...v0.6.0) (2018-02-01)


### Features

* **plugin-media:** Added the media_youtubedl plugin. ([d103dcb](https://gitlab.com/sugarcube/sugarcube/commit/d103dcb)), closes [#16](https://gitlab.com/sugarcube/sugarcube/issues/16)




<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.5.0...v0.5.1) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-youtube

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-youtube

<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)




**Note:** Version bump only for package @sugarcube/plugin-youtube

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)


### Bug Fixes

* Fixed the plugins that got broken by removing bluebird. ([73a9603](https://gitlab.com/sugarcube/sugarcube/commit/73a9603))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-youtube

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-youtube

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.0.0...v0.1.0) (2017-10-08)


### Features

* Imported Youtube plugin. ([958734a](https://gitlab.com/sugarcube/sugarcube/commit/958734a))
