# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)


### Bug Fixes

* **plugin-facebook:** Log progress on info level. ([e50de0e](https://gitlab.com/sugarcube/sugarcube/commit/e50de0e)), closes [#30](https://gitlab.com/sugarcube/sugarcube/issues/30)


### Features

* **plugin-facebook:** Added option to limit the amount of feed messages fetched. ([f8247cd](https://gitlab.com/sugarcube/sugarcube/commit/f8247cd)), closes [#29](https://gitlab.com/sugarcube/sugarcube/issues/29)
* **plugin-facebook:** Fetch the feed of a Facebook page. ([9be0620](https://gitlab.com/sugarcube/sugarcube/commit/9be0620))




<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)


### Bug Fixes

* **plugin-facebook:** Bad API use of dashp. ([4624ace](https://gitlab.com/sugarcube/sugarcube/commit/4624ace))




<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-facebook

<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)




**Note:** Version bump only for package @sugarcube/plugin-facebook

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)




**Note:** Version bump only for package @sugarcube/plugin-facebook

<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-facebook

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-facebook

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.0.0...v0.1.0) (2017-10-08)


### Features

* Imported the facebook plugin. ([f19588d](https://gitlab.com/sugarcube/sugarcube/commit/f19588d))
