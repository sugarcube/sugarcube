# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)


### Features

* **sugarcube:** Added the elasticsearch and workflow package to the distribution. ([6e9ebda](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/commit/6e9ebda))




<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.7.0"></a>
# [0.7.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.6.1...v0.7.0) (2018-02-02)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.6.1"></a>
## [0.6.1](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.6.0...v0.6.1) (2018-02-01)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.5.1...v0.6.0) (2018-02-01)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.5.0...v0.5.1) (2018-01-30)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)


### Features

* **sugarcube:** Added the sugarcube meta package. ([f90faa9](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/commit/f90faa9))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/sugarcube

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)


### Features

* **sugarcube:** Added the sugarcube meta package. ([f90faa9](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/sugarcube/commit/f90faa9))
