# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)




**Note:** Version bump only for package @sugarcube/plugin-twitter

<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)


### Bug Fixes

* **plugin-twitter:** Append the direct link to an image in _sc_media. ([67aaa1a](https://gitlab.com/sugarcube/sugarcube/commit/67aaa1a)), closes [#26](https://gitlab.com/sugarcube/sugarcube/issues/26)
* **plugin-twitter:** Stop loosing fetched tweets when encountering an error. ([045e395](https://gitlab.com/sugarcube/sugarcube/commit/045e395)), closes [#25](https://gitlab.com/sugarcube/sugarcube/issues/25)




<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.5.1...v0.6.0) (2018-02-01)


### Features

* **plugin-twitter:** Scrape extended entities to include videos. ([dab1da7](https://gitlab.com/sugarcube/sugarcube/commit/dab1da7))




<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-twitter

<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)


### Bug Fixes

* **twitter:** Continue the pipeline if an user account is suspended. ([77729c7](https://gitlab.com/sugarcube/sugarcube/commit/77729c7)), closes [#7](https://gitlab.com/sugarcube/sugarcube/issues/7)




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)


### Bug Fixes

* Fixed the plugins that got broken by removing bluebird. ([73a9603](https://gitlab.com/sugarcube/sugarcube/commit/73a9603))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-twitter

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)




**Note:** Version bump only for package @sugarcube/plugin-twitter

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.0.0...v0.1.0) (2017-10-08)


### Features

* Imported the twitter plugin. ([3ebaf5a](https://gitlab.com/sugarcube/sugarcube/commit/3ebaf5a))
