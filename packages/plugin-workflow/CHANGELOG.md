# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-workflow/compare/v0.8.0...v0.9.0) (2018-03-30)


### Features

* **plugin-workflow:** Added the workflow_merge plugin. ([887009a](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-workflow/commit/887009a))
