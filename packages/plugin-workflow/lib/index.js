import mergePlugin from "./plugins/merge";

export const plugins = {
  workflow_merge: mergePlugin,
};

export default {plugins};
