# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.8.0...v0.9.0) (2018-03-30)


### Bug Fixes

* **core:** Load all plugin options correctly. ([ebb1b8b](https://gitlab.com/sugarcube/sugarcube/commit/ebb1b8b))


### Features

* **core,cli:** Added basic facilities for feature toggles. ([8f10385](https://gitlab.com/sugarcube/sugarcube/commit/8f10385))




<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.7.0...v0.8.0) (2018-03-03)


### Bug Fixes

* **core:** Allow state updates to existing paths. ([0b2aca1](https://gitlab.com/sugarcube/sugarcube/commit/0b2aca1))


### Features

* **core:** Added the isThenable predicate. ([e9dae59](https://gitlab.com/sugarcube/sugarcube/commit/e9dae59))
* **core:** Implement custom named curry function. ([121ba34](https://gitlab.com/sugarcube/sugarcube/commit/121ba34))
* **core:** Overload fmap to replace fmapAsync. ([fe641ad](https://gitlab.com/sugarcube/sugarcube/commit/fe641ad))
* **core:** Provide all available plugins to a plugin. ([cf2ebdf](https://gitlab.com/sugarcube/sugarcube/commit/cf2ebdf))
* **core,cli:** Added support for persistent cache. ([ea0429e](https://gitlab.com/sugarcube/sugarcube/commit/ea0429e))




<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.4.0...v0.5.0) (2018-01-30)


### Bug Fixes

* **core:** Added missing argument to function call. ([4476d97](https://gitlab.com/sugarcube/sugarcube/commit/4476d97))


### Features

* **core:** Add state management that can persist over plugin calls. ([87402ad](https://gitlab.com/sugarcube/sugarcube/commit/87402ad))
* **core:** Added md5 hasher to the crypto module. ([19d0a1c](https://gitlab.com/sugarcube/sugarcube/commit/19d0a1c))
* **core:** Defined basic set operations on envelopes. ([dd2618e](https://gitlab.com/sugarcube/sugarcube/commit/dd2618e))
* **core:** Replace shortid dependency with custom uid generator. ([c765395](https://gitlab.com/sugarcube/sugarcube/commit/c765395))
* **core:** Store the pipeline date on every unit of data. ([8da8455](https://gitlab.com/sugarcube/sugarcube/commit/8da8455))
* **core:** Supply plugins with a persistent stats object. ([0374b99](https://gitlab.com/sugarcube/sugarcube/commit/0374b99))




<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.3.0...v0.4.0) (2018-01-12)


### Features

* **core:** Added warn log target. ([8e7f0b4](https://gitlab.com/sugarcube/sugarcube/commit/8e7f0b4))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.3.0) (2017-12-05)


### Bug Fixes

* **core:** Removed failing export of test generators. ([d969881](https://gitlab.com/sugarcube/sugarcube/commit/d969881))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/compare/v0.2.0...v0.2.1) (2017-10-22)




**Note:** Version bump only for package @sugarcube/core

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.1.0...v0.2.0) (2017-10-22)




**Note:** Version bump only for package @sugarcube/core

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/sugarcube/sugarcube/compare/v0.0.0...v0.1.0) (2017-10-08)




**Note:** Version bump only for package @sugarcube/core
