# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-elasticsearch/compare/v0.8.0...v0.9.0) (2018-03-30)


### Features

* **plugin-elasticsearch:** Added basic Elasticsearch related plugins. ([95db5f5](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-elasticsearch/commit/95db5f5))
* **plugin-elasticsearch:** Added the elastic_import_query plugin. ([dbcb02d](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-elasticsearch/commit/dbcb02d))
* **plugin-elasticsearch:** Specify the fields to import. ([9170ef1](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-elasticsearch/commit/9170ef1))
