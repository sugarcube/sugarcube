# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/compare/v0.8.0...v0.9.0) (2018-03-30)




**Note:** Version bump only for package @sugarcube/plugin-mail

<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/compare/v0.7.0...v0.8.0) (2018-03-03)




**Note:** Version bump only for package @sugarcube/plugin-mail

<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/compare/v0.5.0...v0.5.1) (2018-01-30)


### Bug Fixes

* **plugin-mail:** Don't exclude all files in an attempt to include the mail stats template. ([ded4c96](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/commit/ded4c96))




<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/compare/v0.4.0...v0.5.0) (2018-01-30)


### Bug Fixes

* **plugin-mail:** Always return the original envelope after sending diff stats emails. ([3765b0f](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/commit/3765b0f))
* **plugin-mail:** Fixed a wrong variable use in the diff stats email template. ([ec8cbf0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/commit/ec8cbf0))


### Features

* **plugin-mail:** Added the mail_diff_stats plugin. ([c9dd267](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-mail/commit/c9dd267))
