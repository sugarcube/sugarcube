# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.8.0...v0.9.0) (2018-03-30)




**Note:** Version bump only for package @sugarcube/plugin-sec

<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.7.0...v0.8.0) (2018-03-03)




**Note:** Version bump only for package @sugarcube/plugin-sec

<a name="0.7.0"></a>
# [0.7.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.6.1...v0.7.0) (2018-02-02)


### Features

* Consistently use _sc_media over _sc_downloads. ([9c92935](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/commit/9c92935))




<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.5.0...v0.5.1) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-sec

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.4.0...v0.5.0) (2018-01-30)




**Note:** Version bump only for package @sugarcube/plugin-sec

<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.3.0...v0.4.0) (2018-01-12)




**Note:** Version bump only for package @sugarcube/plugin-sec

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.1.0...v0.3.0) (2017-12-05)


### Bug Fixes

* Minor fixes in plugin-regex and plugin-sec. ([cbb6cc0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/commit/cbb6cc0))


### Features

* **plugin-sec:** Imported initial version. ([820b7e9](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/commit/820b7e9))
* **plugin-sec:** Paginate search requests. ([a0053cb](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/commit/a0053cb))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.2.0...v0.2.1) (2017-10-22)


### Bug Fixes

* Minor fixes in plugin-regex and plugin-sec. ([cbb6cc0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/commit/cbb6cc0))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/compare/v0.1.0...v0.2.0) (2017-10-22)


### Features

* **plugin-sec:** Imported initial version. ([820b7e9](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/commit/820b7e9))
* **plugin-sec:** Paginate search requests. ([a0053cb](https://gitlab.com/sugarcube/sugarcube/tree/master/packages/plugin-sec/commit/a0053cb))
